export interface Address {
  buildingName: string;
  format: string;
  postcode: string;
  street: string;
  town: string;
}

export interface Payment {
  month: Number;
  paymentStatus: String;
  year: Number;
}

export interface Account {
  accountCategory: String;
  accountNumber: String;
  address: Address;
  contentKey: String;
  displayName: String;
  key: String;
  name: String;
  overview: {
    lastUpdated: String;
    utilization: Number;
    balance: {
      amount: Number;
      currency: String;
    };
    frequency: String;
    limit: {
      amount: Number;
      currency: String;
    };
    accountOpened: String;
  };
  paymentHistory: [Payment];
  status: String;
  supplierName: String;
}

export interface CreditReport {
  accounts: Account[];
  personal: {
    electoralRoll: [
      {
        address: Address;
        contextKey: String;
        current: Boolean;
        endDateString: String;
        name: String;
        startDateString: String;
        supplied: String;
      }
    ];
    publicInfo: {
      courtAndInsolvencies: [
        {
          name: String;
          dob: String;
          courtName: String;
          contextKey: String;
          dischargeDate: String;
          caseReference: String;
          amount: {
            amount: Number;
            currency: String;
          };
          address: Address;
          type: {
            code: String;
            details: {
              catDesc: String;
            };
          };
          startDate: String;
        }
      ];
    };
  };
  searches: {
    hard: [Search];
    soft: [Search];
  };
}

interface Search {
  address: Address;
  contextKey: String;
  dob: String;
  name: String;
  organisationName: String;
  purpose: String;
  searchDate: String;
}
