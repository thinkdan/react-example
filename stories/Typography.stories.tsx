import * as React from "react";
import { storiesOf } from "@storybook/react";
import { Title, Header, Body } from "../components";

storiesOf("Typography", module).add("Type", () => {
  return (
    <>
      <Title>Title</Title>
      <Header>Header</Header>
      <Body>Body</Body>
    </>
  );
});
