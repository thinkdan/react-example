import styled from "styled-components";

export const Divider = styled.div`
  ${({ theme }) => `
  padding: 10px;
display: block;
width: 100%;
border-top: 1px solid ${theme.colours.grey}
  `}
`;
