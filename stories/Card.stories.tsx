import * as React from "react";
import { storiesOf } from "@storybook/react";
import { Card } from "../components";
import Layout from "../layouts/Main";
import { Container } from "../layouts/Container";

const cardProps = [
  {
    heading: "Electral Roll",
    body: "Being on the electoral roll can improve your score",
    onTrack: true,
    impact: "High Impact"
  },
  {
    heading: "Electral Roll",
    body: "Being on the electoral roll can improve your score",
    onTrack: true,
    impact: "High Impact"
  },
  {
    heading: "Electral Roll",
    body: "Being on the electoral roll can improve your score",
    onTrack: true,
    impact: "High Impact"
  },
  {
    heading: "Electral Roll",
    body: "Being on the electoral roll can improve your score",
    onTrack: true,
    impact: "High Impact"
  },
  {
    heading: "Electral Roll",
    body: "Being on the electoral roll can improve your score",
    onTrack: true,
    impact: "High Impact"
  }
];

storiesOf("Card", module).add("Card", () => {
  const Cards = cardProps.map(cardData => <Card {...cardData} />);

  return (
    <Layout>
      <Container>{Cards}</Container>
    </Layout>
  );
});
