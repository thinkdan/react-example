import * as React from "react";
import { storiesOf } from "@storybook/react";
import { Divider } from "./utility";
import { PrimaryButton, SuccessButton, WarningButton } from "../components";

storiesOf("Button", module)
  .add("Primary", () => {
    return <PrimaryButton text="Button Text" data-testid="button" />;
  })
  .add("Success", () => {
    return (
      <>
        <SuccessButton text="Button Text" small data-testid="button" />
        <Divider />
        <SuccessButton text="Button Text" data-testid="button" />
      </>
    );
  })
  .add("Warning", () => {
    return (
      <>
        <WarningButton text="Button Text" small data-testid="button" />
        <Divider />
        <WarningButton text="Button Text" data-testid="button" />
      </>
    );
  });
