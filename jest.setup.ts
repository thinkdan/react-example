import { configure } from "@testing-library/dom";
configure({ testIdAttribute: "data-testid" });
// Why this is ugly:
// Unfortunately there's an issue with testing react-hooks. It works,
// but there's an ugly warning about things not being wrapped in act()

// Therefore, we're just ignoring those warnings here

const mockConsoleMethod = (realConsoleMethod: any) => {
  const ignoredMessages = [
    "test was not wrapped in act(...)",
    "Warning: componentWillReceiveProps has been renamed",
    "Warning: componentWillMount has been renamed",
    "for a non-boolean attribute",
    "validateDOMNesting"
  ];

  return (message: any, ...args: any) => {
    const containsIgnoredMessage = ignoredMessages.some(ignoredMessage =>
      message.includes(ignoredMessage)
    );

    if (!containsIgnoredMessage) {
      realConsoleMethod(message, ...args);
    }
  };
};
// eslint-disable-next-line
console.warn = jest.fn(mockConsoleMethod(console.warn));

// eslint-disable-next-line
console.error = jest.fn(mockConsoleMethod(console.error));
