import { css } from "styled-components";

const breakPoints = {
  xs: 375,
  sm: 768,
  md: 1024,
  lg: 1280
};

const theme = {
  colours: {
    midnight: "#253648",
    gallery: "#F7F7F8",
    green: "#15693B",
    greenSubtle: "#DDF9EA",
    orange: "#764C25",
    orangeSubtle: "#FDEFE2",
    grey: "#EEEFF1"
  },
  borderRadius: {
    small: "4px",
    large: "8px"
  },
  fontSizes: {
    base: "12px",
    body: "14px",
    header: "16px",
    title: "20px"
  },
  media: Object.keys(breakPoints).reduce((acc, label: string) => {
    //@ts-ignore
    acc[label] = (...args: any) => css`
      //@ts-ignore
      @media (min-width: ${breakPoints[label]}px) {
        //@ts-ignore
        ${css.call(undefined, ...args)};
      }
    `;
    return acc;
  }, {})
};

export default theme;
