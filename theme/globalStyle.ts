import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0px;
    background-color: #EEEFF1; 
      font-family: CSClarity,Helvetica Neue,Helvetica,Arial,sans-serif;
  }
`;
export default GlobalStyle;
