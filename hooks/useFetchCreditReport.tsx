import { useEffect, useState } from "react";
import axios from "axios";
import { CreditReport } from "../types/api";

export const useFetchCreditReport = () => {
  const [result, setResult] = useState<CreditReport>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get("https://api.myjson.com/bins/gap76")
      .then(response => response.data)
      .then(response => {
        setResult(response);
        setLoading(false);
      })
      .catch(error => setError(error));
  }, []);

  return {
    result,
    loading,
    error
  };
};
