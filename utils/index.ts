interface calcCreditUtilisationProps {
  balance: Number;
  limit: Number;
}

export const calcCreditUtilisation = (props: calcCreditUtilisationProps) => {
  const { balance, limit } = props;
  return (+balance / +limit) * 100 > 50 ? true : false;
};
