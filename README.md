# The task

Implement the insights microcosm which appears in the report section. The component will be used to highlight certain aspects of a user's report, both in terms of what is going well and what can be improved.

```
As a user of the platform
When I visit the reports section
I want to be informed of my reports highlights
So that I see how I’m tracking against those insights
```

The project uses NextJs and has been modified to support Typescript,
Styled Components,
Storybook,
React Testing Lib,
Eslint


## How to run

```sh
yarn
yarn run dev
navigate to 'localhost:3000/dashboard'
```

## How to run
_Both_ unit and integration tests run with one command - integration tests live within `__tests__`
```sh
yarn test
```

