import styled from "styled-components";

export const Title = styled.h2`
  ${({ theme }) => `
font-size: ${theme.fontSizes.title};
color: ${theme.colours.midnight};
font-weight: 600;
margin-bottom: 20px
letter-spacing: 0.4px;
  `}
`;

export const Header = styled.h3`
  ${({ theme }) => `
font-size: ${theme.fontSizes.header};
color: ${theme.colours.midnight};
font-weight: 600;
letter-spacing: 0.4px;
margin-bottom:8px
  `}
`;

export const Body = styled.h3`
  ${({ theme }) => `
font-size: ${theme.fontSizes.body};
color: ${theme.colours.midnight};
    margin-top: 8px;
    font-weight: normal;
    line-height: 20px;
    letter-spacing: 0.5px;
  `}
`;
