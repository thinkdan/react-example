import React from "react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { renderWithTheme } from "../../__tests__/utils";
import { Header, Title, Body } from "./index";

describe("Typography", () => {
  test("it renders <Body/> correctly", async () => {
    const container = renderWithTheme(<Body />);
    expect(container).toMatchSnapshot();
  });

  test("it renders <Title/> correctly", async () => {
    const container = renderWithTheme(<Title />);
    expect(container).toMatchSnapshot();
  });

  test("it renders <Header/> correctly", async () => {
    const container = renderWithTheme(<Header />);
    expect(container).toMatchSnapshot();
  });
});
