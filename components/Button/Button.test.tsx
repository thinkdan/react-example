import React from "react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { renderWithTheme } from "../../__tests__/utils";
import { PrimaryButton } from "./index";

describe("Button", () => {
  test("it renders correctly", async () => {
    const container = renderWithTheme(
      <PrimaryButton data-testid="primaryButton" text="button" />
    );
    expect(container).toMatchSnapshot();
  });

  test("it renders text when passed as a prop", async () => {
    const { queryByTestId } = renderWithTheme(
      <PrimaryButton data-testid="primaryButton" text="Present" />
    );
    const primaryButton = queryByTestId("primaryButton");
    expect(primaryButton).toHaveTextContent("Present");
  });

  test.todo("it renders smaller");
  test.todo("it has hover styles");

  // Failed to get jest-styled-components working correctly
  // would test style occurances like so:
  // test("appears as a smaller button when said prop is provided", () => {
  //   const wrapper = renderWithTheme(<PrimaryButton size="small"/>);
  //   expect(wrapper).toHaveStyleRule("padding", "0 15px");
  //   expect(wrapper).toHaveStyleRule("height", "40px");
  //   expect(wrapper).toHaveStyleRule("fontSizes", "40px");
  // });
});
