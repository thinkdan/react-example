import React from "react";
import styled, { css } from "styled-components";

interface ButtonProps {
  small?: boolean;
  text?: string;
  "data-testid"?: string;
  mr?: boolean;
}

export const BaseButton = styled.button<ButtonProps>`
  ${({ theme, mr, small = false }) => css`
    font-family: helvetica;
    border: none;
    border-radius: 0.25rem;
    display: inline-block;
    position: relative;
    text-align: center;
    text-decoration: none;
    white-space: nowrap;
    margin: 0;
    cursor: pointer;
    vertical-align: baseline;
    font-size: ${theme.fontSizes.body};
    padding: 0.75rem 1rem;
    line-height: 1.5;
    letter-spacing: 0.025rem;
    outline: none;
    text-transform: uppercase;

    &:disabled {
      cursor: not-allowed;
      pointer-events: none;
    }

    ${small &&
      css`
        font-size: ${theme.fontSizes.base};
        padding: 0.2rem 0.6rem;
        font-weight: 400;
      `}

    ${mr &&
      css`
        margin-right: 8px;
      `}
  `}
`;

export const PrimaryButtonSetup = styled(BaseButton)`
  ${({ theme }) => css`
    background: ${theme.colours.gallery};
    color: ${theme.colours.midnight};
  `}
`;

export const PrimaryButton = ({
  text,
  small = false,
  "data-testid": testid
}) => (
  <PrimaryButtonSetup data-testid={testid} small={small}>
    {text}
  </PrimaryButtonSetup>
);

const SuccessButtonSetup = styled(BaseButton)<ButtonProps>`
  ${({ theme }) => css`
    background: ${theme.colours.greenSubtle};
    color: ${theme.colours.green};
  `}
`;

export const SuccessButton = ({
  text,
  small = false,
  "data-testid": testid,
  mr = false
}) => (
  <SuccessButtonSetup data-testid={testid} small={small} mr={mr}>
    {text}
  </SuccessButtonSetup>
);

const WarningButtonSetup = styled(BaseButton)<ButtonProps>`
  ${({ theme }) => css`
    background: ${theme.colours.orangeSubtle};
    color: ${theme.colours.orange};
  `}
`;

export const WarningButton = ({
  text,
  small = false,
  "data-testid": testid,
  mr = false
}) => (
  <WarningButtonSetup small={small} data-testid={testid} mr={mr}>
    {text}
  </WarningButtonSetup>
);
