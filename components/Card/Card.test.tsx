import React from "react";
import { renderWithTheme } from "../../__tests__/utils";

import Card from "./index";

describe("Card", () => {
  test("it renders correctly", async () => {
    const container = renderWithTheme(<Card />);
    expect(container).toMatchSnapshot();
  });

  test("it displays on track progression", () => {
    const { getByTestId } = renderWithTheme(<Card onTrack />);
    const onTrackButton = getByTestId("ontrack-button");
    expect(onTrackButton).toBeTruthy();

    // can't re-render without setup
  });

  test("it displays off track progression", () => {
    const { getByTestId } = renderWithTheme(<Card onTrack={false} />);
    const offTrackButton = getByTestId("offtrack-button");
    expect(offTrackButton).toBeTruthy();
  });
});
