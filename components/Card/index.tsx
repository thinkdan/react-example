import React from "react";
import styled from "styled-components";
import {
  Header,
  Body,
  SuccessButton,
  WarningButton,
  PrimaryButton
} from "../index";

interface MainProps {
  heading?: string;
  body?: string;
  onTrack?: boolean;
  loading?: boolean;
  impact?: string;
}

const buttonSwitch = onTrack =>
  onTrack ? (
    <SuccessButton small data-testid="ontrack-button" text="On Track" mr />
  ) : (
    <WarningButton small data-testid="offtrack-button" text="Off Track" mr />
  );

// should be own <Card /> component
export const Outer = styled.div`
  padding: 22px;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
  background-color: #fff;
  border-radius: 0.45rem;
`;

// should live within 'page' subfolder as it's
// implementation and not part of component lib
const Main: React.SFC<MainProps> = ({
  heading,
  body,
  onTrack = true,
  impact
}) => {
  return (
    <Outer data-testid="card">
      {buttonSwitch(onTrack)}
      <PrimaryButton small data-testid="impact-button" text={impact} />
      <Header>{heading}</Header>
      <Body>{body}</Body>
    </Outer>
  );
};

export default Main;
