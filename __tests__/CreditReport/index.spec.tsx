import React from "react";
import App from "../../pages/_app";
import axios from "axios";
import dashboard from "../../pages/dashboard";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { Router } from "react-router-dom";
import { render, wait } from "@testing-library/react";

jest.mock("axios");

describe("Dashboard", () => {
  // Mock App
  const url = "https://api.myjson.com/bins/gap76";
  const app = <App router={Router} pageProps={{}} Component={dashboard} />;

  test("User can see loading spinner when the page is loading", async () => {
    const { getByTestId, queryByTestId } = render(app);
    const loadingSpinner = getByTestId("loading-spinner");

    expect(axios.get).toHaveBeenCalled();
    expect(axios.get).toHaveBeenCalledWith(url);
    expect(loadingSpinner).toBeInTheDocument();
    await wait(() => expect(queryByTestId("loading-spinner")).toBeNull());
  });

  test("User can see insights with cards when the page had loaded", async () => {
    const { queryByTestId, getByTestId, queryAllByTestId } = render(app);
    expect(queryByTestId("insights")).not.toBeInTheDocument();

    await wait(() => expect(queryAllByTestId("card")).toHaveLength(3));
    await wait(() => expect(getByTestId("insights")).toBeInTheDocument());
  });

  test.todo("User can see error state when request fails ");
  test.todo("User can see tracking progress of each insights");
});
