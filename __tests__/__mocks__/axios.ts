import { AxiosResponse } from "axios";
import { creditReport } from "./creditReport";


const axiosResponse: AxiosResponse = {
  data: creditReport,
  status: 200,
  statusText: "OK",
  config: {},
  headers: {}
};

export default {
  default: {
    get: jest.fn().mockImplementation(() => Promise.resolve(axiosResponse))
  },
  get: jest.fn(() => Promise.resolve(axiosResponse))
};
