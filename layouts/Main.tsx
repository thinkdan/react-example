import React from "react";
import styled, { css } from "styled-components";

interface LayoutProps {
  "data-qaid"?: string;
}

export const Layout = styled.div<LayoutProps>`
  ${({ theme }) => css`
    margin: 0 auto;
    padding: 0 20px;
    margin-top: 40px;

    ${theme.media.sm`
      max-width: 540px;
      padding: 0 10px;
    `}

    ${theme.media.sm`
      max-width: 740px;
    `}

    ${theme.media.md`
      max-width:960px;
    `}

    ${theme.media.lg`
      max-width: 1160px;
    `}
  `}
`;

const MainLayout: React.SFC = ({ children }) => (
  <Layout data-qaid="layout">{children}</Layout>
);

export default MainLayout;
