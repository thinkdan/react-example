import styled, { css } from "styled-components";

export const Container = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-column-gap: 10px;
    grid-row-gap: 10px;
    grid-template-columns: 1fr 1fr;

    ${theme.media.xs`
  grid-template-columns: 1fr 1fr;
  `}

    ${theme.media.sm`
  grid-template-columns: 1fr 1fr;
    grid-column-gap: 15px;
    grid-row-gap: 15px;
  `}

    ${theme.media.md`
  grid-template-columns: 1fr 1fr 1fr;
  `}
  `}
`;
