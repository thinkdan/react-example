import { configure, addDecorator } from "@storybook/react";
import theme from "../theme";
import GlobalStyle from "../theme/globalStyle";
import React from "react";
import { ThemeProvider } from "styled-components";

addDecorator(story => (
  <>
    <GlobalStyle />
    <ThemeProvider theme={theme}>{story()}</ThemeProvider>
  </>
));

configure(require.context("../stories", true, /\.stories\.tsx?$/), module);
