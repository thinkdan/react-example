import React from "react";
import { useFetchCreditReport } from "../hooks/useFetchCreditReport";
import { Title, Card, Spinner } from "../components";
import { Container } from "../layouts/Container";
import { calcCreditUtilisation } from "../utils";

const Index: React.SFC = ({}) => {
  const { result: creditReport, loading, error } = useFetchCreditReport();
  if (loading || !creditReport) {
    return <Spinner center data-testid="loading-spinner" />;
  }

  if (error || !creditReport) {
    return <h2>Error</h2>;
  }

  // Destructure Response
  const {
    personal: {
      publicInfo: { courtAndInsolvencies },
      electoralRoll: electoralRollArray
    },
    accounts
  } = creditReport;

  // Credit utilisation
  const accountCreditCard = accounts.filter(
    acc => acc.accountCategory == "credit_cards"
  );

  const {
    balance: { amount: balance },
    limit: { amount: limit }
  } = accountCreditCard[0].overview;

  // Insight Trackers Values
  const creditUtilisation = calcCreditUtilisation({
    balance,
    limit
  });
  const electoralRoll = !!electoralRollArray.filter(e => e.current);
  const publicInformation = !!courtAndInsolvencies.length;

  return (
    <div data-testid="insights">
      <Title>Insights</Title>
      <Container>
        <Card
          onTrack={publicInformation}
          loading={loading}
          heading="Public information"
          body="Bankruptcies and individual voluntary arrangements can damage your score"
          impact="High Impact"
        />
        <Card
          onTrack={creditUtilisation}
          heading="Credit utilisation"
          body="Using more than 50% of your available credit can damage your score"
          impact="Medium Impact"
        />
        <Card
          onTrack={electoralRoll}
          heading="Electoral roll"
          body="Being on the electoral roll can improve your score"
          impact="High Impact"
        />
      </Container>
    </div>
  );
};

export default Index;

// // Credit utilisation: "refactor"
// for (let i = 0; i < accountArray.length; i++) {
//   if (accountArray[i].accountCategory == "credit_cards") {
//     const {
//       balance: { amount: accountBalance },
//       limit: { amount: accountAmount }
//     } = accountArray[i].overview;

//     // @ts-ignore
//     const creditUtilisation = calcCreditUtilisation({
//       accountBalance,
//       accountAmount
//     });
//     break
//   }
// }
