import React from "react";
import Link from "next/link";

const Index: React.SFC = ({}) => {
  return (
    <Link href="/dashboard">
      <a>Visit dashboard</a>
    </Link>
  );
};

export default Index;
