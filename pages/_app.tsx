import { AppProps } from "next/app";
import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "../theme";
import GlobalStyle from "../theme/globalStyle";
import Layout from "../layouts/Main";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </ThemeProvider>
    </>
  );
}

export default MyApp;
